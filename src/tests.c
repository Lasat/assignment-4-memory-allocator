#include "tests.h"
#include "mem.h"
#include "mem_internals.h"

static struct block_header *get_block_header(void *ptr) {
    return (struct block_header *) (ptr - offsetof(struct block_header, contents));
}

static bool test_alloc(void** alloc, size_t alloc_size) {
    *alloc = _malloc(alloc_size);
    if(!*alloc) return false;
    const struct block_header *alloc_header = get_block_header(*alloc);
    if(alloc_header->is_free||alloc_header->capacity.bytes!=alloc_size) return false;
    return true;
}

static bool test_free(void *alloc) {
    _free(alloc);
    if(!get_block_header(alloc)->is_free) return false;
    return true;
}

#define return_fail {         \
    printf("\nTest failed\n"); \
    return false; }

#define return_success {         \
    printf("\nTest passed\n"); \
    return true; }

bool test_1() {
    printf("Test 1, default operation\n");
    heap_init(1);
    void* alloc;
    if(!test_alloc(&alloc, 64)) return_fail
    debug_heap(stdout, HEAP_START);
    if(!test_free(alloc)) return_fail
    munmap(HEAP_START, 8192);
    printf("\nTest passed!\n");
    return_success
}

bool test_2() {
    printf("Test 2, freeing the memory block\n");
    heap_init(1);
    void *alloc, *alloc2;
    if(!test_alloc(&alloc,32)||!test_alloc(&alloc2,64)) return_fail
    debug_heap(stdout, HEAP_START);
    if(!test_free(alloc)) return_fail
    debug_heap(stdout, HEAP_START);
    if(!test_free(alloc2)) return_fail
    munmap(HEAP_START, 8192);
    return_success
}

bool test_3() {
    printf("Test 3, memory region expansion\n");
    heap_init(1);
    debug_heap(stdout, HEAP_START);
    void* alloc;
    if(!test_alloc(&alloc, 8192)) return_fail
    debug_heap(stdout, HEAP_START);
    if(!test_free(alloc)) return_fail
    munmap(HEAP_START, 20480);
    return_success
}
