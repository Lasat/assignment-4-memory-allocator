#include <stdio.h>
#include <string.h>

#include "tests.h"

int main(void) {
    size_t tests_passed = 0;

    tests_passed += test_1();
    tests_passed += test_2();
    tests_passed += test_3();


    if(tests_passed == TESTS_LENGTH) {
        printf("All tests passed successfully!\n");
    } else if(tests_passed!=0) {
        printf("Some tests failed");
    } else {
        printf("All tests failed");
    }
    return 0;
}
