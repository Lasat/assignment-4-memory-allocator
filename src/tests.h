#ifndef TESTS_H
#define TESTS_H

#include <stdbool.h>

#define TESTS_LENGTH 3

bool test_1();

bool test_2();

bool test_3();

#endif
